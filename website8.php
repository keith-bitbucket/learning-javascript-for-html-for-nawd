<?php
session_start();

if (empty($_SESSION['count'])) $_SESSION['count']=0;

++$_SESSION['count'];

switch ($_SESSION['count']) {
    case 1:
        echo "Thanks!  The server is now calling {$_GET['name']} at {$_GET['phone']}.";
        break;
    case 2:
        echo "Please hold while I connect you.";
        break;
    case 3:
        echo "Please be patient.";
        break;
    default:
        echo "Watch it buddy or I'll switch your long distance carrier!";
        break;
}


?>
